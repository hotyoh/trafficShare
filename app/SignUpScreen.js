
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AWS from 'aws-sdk/dist/aws-sdk-react-native';

var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider(
  {
    region: "us-east-1",
    identity_pool_id: "us-east-1_QpLR8vnbW"
  });

const background = require("../screens/signup/signup_bg.png");
const backIcon = require("../screens/signup/back.png");
const personIcon = require("../screens/signup/signup_person.png");
const lockIcon = require("../screens/signup/signup_lock.png");
const emailIcon = require("../screens/signup/signup_email.png");
const birthdayIcon = require("../screens/signup/signup_birthday.png");

export default class SignUpScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      username: '',
      email: '',
      password: ''
    };


    this._onPress = this._onPress.bind(this);
    
  }
  _onPress() {
    var params = {
      ClientId: '6qkk62ap5esfji3pu6v5jrhrvq', /* Fixed from Cognito */
      Password: this.state.password,
      Username: this.state.username,
      UserAttributes: [
        { Name: 'email', Value: this.state.email },
        { Name: 'name', Value: this.state.username }
      ],
      ValidationData: []
    };
    console.log('signUp : ', params);
    var _self = this;
    cognitoidentityserviceprovider.signUp(params, function (err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
      }
      else {
        console.log(data);           // successful response
        _self.props.navigation.navigate('ConfirmCode');
      }
    });
  }

  render() {

    return (

      <View style={styles.container}>
        <Image source={background} style={[styles.container, styles.bg]} resizeMode="cover">
          <View style={styles.headerContainer}>

            <View style={styles.headerIconView}>
              <TouchableOpacity style={styles.headerBackButtonView} onPress={() => this.props.navigation.navigate('SignIn', { })}>
                <Image source={backIcon} style={styles.backButtonIcon} resizeMode="contain" />
              </TouchableOpacity>
            </View>

            <View style={styles.headerTitleView}>
              <Text style={styles.titleViewText}>Sign Up</Text>
            </View>

          </View>

          <View style={styles.inputsContainer}>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image source={personIcon} style={styles.inputIcon} resizeMode="contain" />
              </View>
              <TextInput
                style={[styles.input, styles.whiteFont]}
                placeholder="Username"
                placeholderTextColor="#FFF"
                underlineColorAndroid='transparent'
                onChangeText={(username) => this.setState({ username })}
              />
            </View>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image source={emailIcon} style={styles.inputIcon} resizeMode="contain" />
              </View>
              <TextInput
                style={[styles.input, styles.whiteFont]}
                placeholder="Email"
                placeholderTextColor="#FFF"
                onChangeText={(email) => this.setState({ email })}
              />
            </View>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image source={lockIcon} style={styles.inputIcon} resizeMode="contain" />
              </View>
              <TextInput
                secureTextEntry={true}
                style={[styles.input, styles.whiteFont]}
                placeholder="Password"
                placeholderTextColor="#FFF"
                onChangeText={(password) => this.setState({ password })}
              />
            </View>

          </View>

          <View style={styles.footerContainer}>

            <TouchableOpacity onPress={this._onPress}>
              <View style={styles.signup}>
                <Text style={styles.whiteFont}>Join</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity>
              <View style={styles.signin}>
                <Text style={styles.greyFont}>Already have an account?<Text style={styles.whiteFont} onPress={() => this.props.navigation.navigate('SignIn', { })}> Sign In</Text></Text>
              </View>
            </TouchableOpacity>
          </View>
        </Image>
      </View>
    );
  }
}


let styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bg: {
    paddingTop: 30,
    width: null,
    height: null
  },
  headerContainer: {
    flex: 1,
  },
  inputsContainer: {
    flex: 3,
    marginTop: 50,
  },
  footerContainer: {
    flex: 1
  },
  headerIconView: {
    marginLeft: 10,
    backgroundColor: 'transparent'
  },
  headerBackButtonView: {
    width: 25,
    height: 25,
  },
  backButtonIcon: {
    width: 25,
    height: 25
  },
  headerTitleView: {
    backgroundColor: 'transparent',
    marginTop: 25,
    marginLeft: 25,
  },
  titleViewText: {
    fontSize: 30,
    color: '#ecf0f1',
  },
  inputs: {
    paddingVertical: 20,
  },
  inputContainer: {
    borderWidth: 1,
    borderBottomColor: '#CCC',
    borderColor: 'transparent',
    flexDirection: 'row',
    height: 75,
  },
  iconContainer: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputIcon: {
    width: 30,
    height: 30,
  },
  input: {
    flex: 1,
    fontSize: 20,
  },
  signup: {
    backgroundColor: '#FF3366',
    paddingVertical: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
  },
  signin: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  greyFont: {
    color: '#D8D8D8'
  },
  whiteFont: {
    color: '#FFF'
  }
})
